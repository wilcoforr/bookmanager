﻿using System.Collections.Generic;
using System.Web.Mvc;

using BookManager.DataAccess;

namespace BookManager.ViewModels
{
    public class BookViewModel
    {
        public Book Book { get; set; }


        /// <summary>
        /// Get a list of Authors
        /// </summary>
        public IEnumerable<SelectListItem> Authors
        {
            get
            {
                return new SelectList(
                        new BookManagerEntities().Authors,
                        nameof(Author.AuthorID),
                        nameof(Author.FullName)
                    );
            }
        }

    }
}