﻿using BookManager.DataAccess;
using System.Collections.Generic;
using System.Web.Mvc;

namespace BookManager.ViewModels
{
    public class TagBookViewModel
    {
        public int BookID { get; set; }

        public int TagID { get; set; }

        public IEnumerable<SelectListItem> Tags
        {
            get
            {
                return new SelectList(
                        new BookManagerEntities().Tags,
                        nameof(Tag.TagID),
                        nameof(Tag.TagContent)
                    );
            }
        }

    }
}