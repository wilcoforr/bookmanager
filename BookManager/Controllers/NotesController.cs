﻿using System.Web.Mvc;

using BookManager.DataAccess;

namespace BookManager.Controllers
{
    public class NotesController : BaseController
    {
        /// <summary>
        /// Create a note for a book
        /// </summary>
        /// <param name="bookID"></param>

        [HttpGet]
        public ViewResult Create(int bookID)
        {
            var note = new Note();

            note.Book = bookDB.Books.Find(bookID);

            return View(note);
        }

        /// <summary>
        /// Save the new note to the database
        /// </summary>
        /// <param name="note"></param>

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Note note)
        {
            bookDB.Notes.Add(note);

            bookDB.SaveChanges();

            return RedirectToAction("Details", "Books", new { bookID = note.BookID });
        }


        /// <summary>
        /// Delete a Note from the database
        /// </summary>
        /// <param name="noteID"></param>
        [HttpPost]
        public ActionResult Delete(int noteID)
        {
            var note = bookDB.Notes.Find(noteID);

            bookDB.Notes.Remove(note);

            bookDB.SaveChanges();

            return RedirectToAction("Details", "Books", new { bookID = note.BookID });
        }


    }
}