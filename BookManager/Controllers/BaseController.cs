﻿using System.Web.Mvc;

using BookManager.DataAccess;

namespace BookManager.Controllers
{
    /// <summary>
    /// All controllers will inherit from this Base controller.
    /// Mainly for creating an instance of the entities for data access, and
    /// also disposing the entities when the Controller is done doing work.
    /// </summary>
    public abstract class BaseController : Controller
    {

        /// <summary>
        /// BookManager database entities/context
        /// </summary>
        protected readonly BookManagerEntities bookDB = new BookManagerEntities();

        /// <summary>
        /// Dispose the book database entities/context for memory cleanup
        /// </summary>
        /// <param name="disposing"></param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                bookDB.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}