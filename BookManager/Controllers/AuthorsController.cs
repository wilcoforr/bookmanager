﻿using System.Linq;
using System.Web.Mvc;

using BookManager.DataAccess;

namespace BookManager.Controllers
{
    public class AuthorsController : BaseController
    {
        /// <summary>
        /// Create an author
        /// </summary>
        [HttpGet]
        public ActionResult Create()
        {
            var author = new Author();

            return View(author);
        }

        /// <summary>
        /// Save the Created Author to the database
        /// </summary>
        /// <param name="author"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Author author)
        {
            if (!ModelState.IsValid)
            {
                return View(author);
            }

            bookDB.Authors.Add(author);

            bookDB.SaveChanges();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// View Details of an Author
        /// </summary>
        /// <param name="authorID"></param>
        [HttpGet]
        public ActionResult Details(int authorID)
        {
            var author = bookDB.Authors.Find(authorID);

            return View(author);
        }

        /// <summary>
        /// Edit an Author
        /// </summary>
        /// <param name="authorID"></param>
        [HttpGet]
        public ActionResult Edit(int authorID)
        {
            var author = bookDB.Authors.Find(authorID);

            return View(author);
        }

        /// <summary>
        /// Save the Edits of the Author to the database
        /// </summary>
        /// <param name="author"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Author author)
        {
            if (!ModelState.IsValid)
            {
                return View(author);
            }

            bookDB.Entry(author).State = System.Data.Entity.EntityState.Modified;
            bookDB.SaveChanges();

            return RedirectToAction("Index");
        }

        /// <summary>
        /// Delete an Author
        /// </summary>
        /// <param name="authorID"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public void Delete(int authorID)
        {
            var author = bookDB.Authors.Find(authorID);

            bookDB.Authors.Remove(author);

            bookDB.SaveChanges();
        }

    }
}