﻿using System.Data.Entity;
using System.Web.Mvc;

using BookManager.DataAccess;
using BookManager.ViewModels;

namespace BookManager.Controllers
{
    public class BooksController : BaseController
    {
        /// <summary>
        /// Create a book
        /// </summary>
        [HttpGet]
        public ActionResult Create()
        {
            return View(new BookViewModel());
        }

        /// <summary>
        /// Save the created book to the database
        /// </summary>
        /// <param name="bookVM"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(BookViewModel bookVM)
        {
            if (!ModelState.IsValid)
            {
                return View(bookVM);
            }

            bookDB.Books.Add(bookVM.Book);

            bookDB.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Show a Book's details
        /// </summary>
        /// <param name="bookID"></param>
        [HttpGet]
        public ActionResult Details(int bookID)
        {
            var book = bookDB.Books.Find(bookID);

            return View(book);
        }


        /// <summary>
        /// Edit a Book details
        /// </summary>
        /// <param name="bookID"></param>
        [HttpGet]
        public ActionResult Edit(int bookID)
        {
            var book = bookDB.Books.Find(bookID);

            return View(book);
        }

        /// <summary>
        /// Save the edits to the book to the database
        /// </summary>
        /// <param name="book"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Book book)
        {
            if (!ModelState.IsValid)
            {
                return View(book);
            }

            bookDB.Entry(book).State = EntityState.Modified;
            bookDB.SaveChanges();

            return RedirectToAction("Details", new { bookID = book.BookID });
        }

        /// <summary>
        /// Delete a book
        /// </summary>
        /// <param name="bookID"></param>

        [HttpGet]
        public ActionResult Delete(int bookID)
        {
            return View(bookDB.Books.Find(bookID));
        }

        /// <summary>
        /// Delete a book from the database
        /// </summary>
        /// <param name="bookID"></param>
        [ActionName("Delete")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int bookID)
        {
            var bookToDelete = bookDB.Books.Find(bookID);

            bookDB.Books.Remove(bookToDelete);

            bookDB.SaveChanges();

            return RedirectToAction("Index", "Home");
        }


    }
}
