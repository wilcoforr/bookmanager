﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

using BookManager.DataAccess;

namespace BookManager.Controllers
{
    public class HomeController : BaseController
    {
        /// <summary>
        /// View all books in the database. Not in the Books controller because this 
        /// is the main "view" of all the data in the web application
        /// </summary>
        /// <param name="searchText">search text</param>
        /// <param name="searchOption">search option</param>
        /// <param name="sortOption">sort option</param>

        public ActionResult Index(string searchText = "", string searchOption = "", string sortOption = "titleAsc")
        {
            var books = bookDB.Books.AsNoTracking().AsQueryable();

            if (string.IsNullOrWhiteSpace(searchText))
            {
                //return all books
                return View(SortBooks(books, sortOption));
            }

            //search is by book title
            if (searchOption == "title")
            {
                books = books.Where(b => b.Title.Contains(searchText));

                return View(SortBooks(books, sortOption));
            }
            //search by the tags entered. can be comma separated for searching for more than one tag
            //example: "ww2, biography" or "fantasy, short story"
            else if (searchOption == "tags")
            {
                var booksByTag = new List<Book>();

                //convert to uppercase for .Contains() later
                //Note: .Contains() is case sensitive. Could rewrite this to be a Regex instead.
                IEnumerable<string> splitSearchString = searchText.Split(',').Select(s => s.Trim().ToUpper());

                foreach (var book in books)
                {
                    foreach (string tag in book.GetTagsAsCsvString().Split(',').Select(s => s.Trim().ToUpper()))
                    {
                        foreach (string splitString in splitSearchString)
                        {
                            if (tag.Contains(splitString))
                            {
                                booksByTag.Add(book);
                            }
                        }
                    }

                }

                return View(SortBooks(booksByTag.Distinct(), sortOption));

            }

            return View(SortBooks(books, sortOption));
        }

        /// <summary>
        /// Utility method to sort books
        /// </summary>
        /// <param name="books"></param>
        /// <param name="sortOption"></param>

        private IEnumerable<Book> SortBooks(IEnumerable<Book> books, string sortOption)
        {
            switch (sortOption)
            {
                case "titleAsc":
                    return books.OrderBy(b => b.Title);
                case "titleDesc":
                    return books.OrderByDescending(b => b.Title);
                case "authorAsc":
                    return books.OrderBy(b => b.Author.FullName)
                                .ThenBy(b => b.Title);
                case "authorDesc":
                    return books.OrderByDescending(b => b.Author.FullName)
                                .ThenBy(b => b.Title);
                //note this is "reversed" for notecount because I want the Ascending to show the books with the most notes
                case "noteCountAsc":
                    return books.OrderByDescending(b => b.Notes.Count)
                                .ThenBy(b => b.Title);
                case "noteCountDesc":
                    return books.OrderBy(b => b.Notes.Count)
                                .ThenBy(b => b.Title);
                default:
                    return books.OrderBy(b => b.Title);
            }
        }

        [HttpGet]
        public ViewResult About()
        {
            return View();
        }
    }
}