﻿using System.Linq;
using System.Web.Mvc;

using BookManager.DataAccess;
using BookManager.ViewModels;

namespace BookManager.Controllers
{
    public class TagsController : BaseController
    {
        /// <summary>
        /// Create a tag
        /// </summary>
        [HttpGet]
        public ActionResult Create()
        {
            return View(new Tag());
        }

        /// <summary>
        /// Save the created tag to the database
        /// </summary>
        /// <param name="tag"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Tag tag)
        {
            bookDB.Tags.Add(tag);

            bookDB.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        /// <summary>
        /// Add a Tag to a book
        /// </summary>
        /// <param name="bookID"></param>
        [HttpGet]
        public ActionResult AddTagToBook(int bookID)
        {
            var tbvm = new TagBookViewModel();

            tbvm.BookID = bookID;

            return View(tbvm);
        }

        /// <summary>
        /// Save the added tag to a book to the database
        /// </summary>
        /// <param name="tbvm"></param>
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AddTagToBook(TagBookViewModel tbvm)
        {
            var btt = bookDB.BookToTags.Create();

            btt.BookID = tbvm.BookID;
            btt.TagID = tbvm.TagID;

            bookDB.BookToTags.Add(btt);
            bookDB.SaveChanges();

            return RedirectToAction("Details", "Books", new { bookID = tbvm.BookID });
        }



        /// <summary>
        ///just a simple HttpGet method to remove a tag from a book.
        ///in a production/real app example, this would be an HttpPost so that someone couldnt just spam
        ///a bunch of requests to remove all tags from books.
        /// </summary>
        /// <param name="bookID"></param>
        /// <param name="tagID"></param>
        [HttpGet]
        public ActionResult RemoveTag(int bookID, int tagID)
        {
            var bookToTag = bookDB.BookToTags.FirstOrDefault(btt => btt.BookID == bookID && btt.TagID == tagID);

            bookDB.BookToTags.Remove(bookToTag);

            bookDB.SaveChanges();

            return RedirectToAction("Details", "Books", new { bookID = bookID });
        }

    }
}