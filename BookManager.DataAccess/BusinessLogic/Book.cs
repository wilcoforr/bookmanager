﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BookManager.DataAccess
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(BookMetadata))]
    public partial class Book
    {
        public string GetTagsAsCsvString()
        {
            var tags = BookToTags.Select(btt => btt.Tag).Select(t => t.TagContent);

            return string.Join(", ", tags);
        }
    }

    /// <summary>
    /// Metadata for ASP MVC HTML Helpers
    /// </summary>
    public class BookMetadata
    {
        [DisplayName("Year Published")]
        public int? YearPublished { get; set; }
        [DisplayName("Date Started")]
        public DateTime? DateStarted { get; set; }
        [DisplayName("Date Finished")]
        public DateTime? DateFinished { get; set; }
    }

}