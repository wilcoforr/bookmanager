﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BookManager.DataAccess
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(TagMetaData))]
    public partial class Tag
    {
    }

    public class TagMetaData
    {
        [DisplayName("Tag Content")]
        [System.ComponentModel.DataAnnotations.MaxLength(100)]
        public string TagContent { get; set; }
    }

}