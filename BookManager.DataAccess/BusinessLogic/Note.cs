﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BookManager.DataAccess
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(NoteMetaData))]
    public partial class Note
    {
    }

    public class NoteMetaData
    {
        [DisplayName("Note Content")]
        [System.ComponentModel.DataAnnotations.MaxLength(2000)]
        public string NoteContent { get; set; }
    }
}