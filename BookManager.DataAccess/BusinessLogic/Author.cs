﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace BookManager.DataAccess
{
    [System.ComponentModel.DataAnnotations.MetadataType(typeof(AuthorMetaData))]
    public partial class Author
    {
        public string FirstName()
        {
            return FullName.Split(' ').First();
        }

        public string LastName()
        {
            return FullName.Split(' ').Last();
        }
    }

    public class AuthorMetaData
    {
        [DisplayName("Full Name")]
        [System.ComponentModel.DataAnnotations.MaxLength(200)]
        public string FullName { get; set; }

        [DisplayName("Biography/Notes")]
        [System.ComponentModel.DataAnnotations.MaxLength(4000)]
        public string BiographyNotes { get; set; }
    }
}