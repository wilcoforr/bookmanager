
-- create BookManagerDB

USE BookManager

--DROP TABLE BookToUser
--DROP TABLE [User]

DROP TABLE BookToTag
DROP TABLE Tag
DROP TABLE Note
DROP TABLE Book
DROP TABLE Author

CREATE TABLE Author (
    AuthorID int IDENTITY(1,1) PRIMARY KEY,
    FullName varchar(200) NOT NULL,
    BiographyNotes varchar(4000) NOT NULL
)

CREATE TABLE Book (
    BookID int IDENTITY(1,1) PRIMARY KEY,
    AuthorID int NOT NULL,
    Title varchar(100) NOT NULL,
    YearPublished int NULL,
    DateStarted datetime2 NULL,
    DateFinished datetime2 NULL,
    Review varchar(4000) NULL,
    Rating int NULL
)

ALTER TABLE [dbo].[Book]  WITH CHECK ADD  CONSTRAINT [FK_Book_Author_AuthorID] FOREIGN KEY([AuthorID])
REFERENCES [dbo].[Author] ([AuthorID])
ON DELETE CASCADE
GO


CREATE TABLE Tag (
    TagID int IDENTITY(1,1) PRIMARY KEY,
    TagContent varchar(100) NOT NULL
)

CREATE TABLE BookToTag (
    BookToTagID int IDENTITY(1,1) PRIMARY KEY,
    BookID int NOT NULL,
    TagID int NOT NULL
)

ALTER TABLE [dbo].[BookToTag]  WITH CHECK ADD  CONSTRAINT [FK_Book_BookToTag_BookID] FOREIGN KEY([BookID])
REFERENCES [dbo].[Book] ([BookID])
ON DELETE CASCADE
GO


ALTER TABLE [dbo].[BookToTag]  WITH CHECK ADD  CONSTRAINT [FK_Tag_BookToTag_TagID] FOREIGN KEY([TagID])
REFERENCES [dbo].[Tag] ([TagID])
ON DELETE CASCADE
GO


CREATE TABLE Note (
    NoteID int IDENTITY(1,1) PRIMARY KEY,
    BookID int NOT NULL,
    NoteContent varchar(2000) NOT NULL,
    PageNumber int NULL
)

ALTER TABLE [dbo].[Note]  WITH CHECK ADD  CONSTRAINT [FK_Note_Book_BookID] FOREIGN KEY([BookID])
REFERENCES [dbo].[Book] ([BookID])
ON DELETE CASCADE
GO


--CREATE TABLE [User] (
--    UserID int IDENTITY(1,1) PRIMARY KEY,
--    UserName varchar(20) NOT NULL,
--    DateCreated datetime2 NOT NULL DEFAULT GETDATE()
--)

--CREATE TABLE BookToUser (
--    BookToUserID int IDENTITY(1,1) NOT NULL,
--    BookID int NOT NULL,
--    UserID int NOT NULL
--)


--ALTER TABLE [dbo].[BookToUser]  WITH CHECK ADD  CONSTRAINT [FK_User_BookToUser_UserID] FOREIGN KEY([UserID])
--REFERENCES [dbo].[User] ([UserID])
--ON DELETE CASCADE
--GO

--ALTER TABLE [dbo].[BookToUser] WITH CHECK ADD  CONSTRAINT [FK_Book_BookToUser_BookID] FOREIGN KEY([BookID])
--REFERENCES [dbo].[Book] ([BookID])
--ON DELETE CASCADE
--GO




INSERT INTO Author
(FullName, BiographyNotes)
VALUES
('Saburo Sakai', 'World War Two Japanese Imperial Naval Force fighter pilot. Fought in South Pacific seas against primarily the British Empire, Australia, and United States of America.')
,('Adam Makos', 'Adam Makos (born 1981) is an American writer, editor, historian and publisher. Makos specializes in war novels and biographies appearing on the New York Times Best Seller list twice for his works Spearhead and A Higher Call. While in high school Makos was one of the founding members of Valor a magazine depicting veterans stories ranging from World War I to Iraq War. - Wikipedia')
,('Spenser Wilkinson', 'Henry Spenser Wilkinson (1 May 1853 in Hulme, Manchester � 31 January 1937 in Oxford) was the first Chichele Professor of Military History at Oxford University. While he was an English writer known primarily for his work on military subjects, he had wide interests. Earlier in his career he was the drama critic for London''s Morning Post. - Wikipedia')
,('J. E. Johnson', 'World War Two fighter pilot of the Royal Air Force (RAF) for the British Empire.')

INSERT INTO Book
(AuthorID, Title, YearPublished, DateStarted, DateFinished, Review, Rating)
VALUES
(1, 'Samurai!', 1999, GETDATE()-30, GETDATE(), 'Auto biography of one of Japan''s leading fighter pilot aces in WW2.', 8)
,(2, 'Spearhead', 1922, GETDATE()-50, NULL, 'A WW2 Tank battle book of the Allies versus the Germans in Normandy in 1944 to the closing months of the Western Front in 1945. Not a great book. Did not finish. Almost written like a long Twitter thread - almost every alternating paragraph was composed of one or two sentences.', 4)
,(3, 'Britain at Bay', 1909, null, null, 'Book about the naval arms race between Germany and Britain', null)
,(4, 'Wing Leader', 1955, null, null, 'A recount of Group Captain J. E. Jackson in the Royal Air Force in World War Two against German Airforces (Luftwaffe).', null)

INSERT INTO Note 
(NoteContent, BookID, PageNumber)
VALUES
('During the 1930s Imperial Japan only trained 100 or so fighter pilots a year under a VERY rigorous program.', 1, 16)
,('Japanese opinion on the war in China was that it was very much like the Western powers fighting in Korea in the 1950s, as a "security intervention"', 1, 22)
,('Sinking of the Haruna was US/Allies propaganda by Captain Colin Kelly, as this was reported to sink in 1941, however it really sunk in July 1945.', 1, 61)
,('In the preface, an interesting experience happened when Johnson met a German pilot that after the German pilot surrendered to the Western Allies (British and American troops) that he thought the Western Allies would re-arm his plane so that he could fight USSR in the east.', 4, 2)


INSERT INTO Tag
(TagContent)
VALUES
('World War One') --1
,('World War Two') --2
,('Vietnam War') --3
,('Korean War') --4
,('Cold War') --5
,('Non-Fiction') --6
,('Fiction') --7
,('Fighter pilot') --8
,('Soldier') --9
,('War') --10
,('Biography') --11
,('Autobiography') --12
,('History') --13
,('Fantasy') --14
,('Short Story Collection') --14


INSERT INTO BookToTag
(BookID, TagID)
VALUES
(1, 2)
,(1, 6)
,(1,8)
,(2,2)
,(2, 9)
,(2, 6)
,(3, 6)





